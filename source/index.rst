.. profile documentation master file, created by
   sphinx-quickstart on Sun Aug 22 11:17:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to my profile's documentation!
======================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   projects/index.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
